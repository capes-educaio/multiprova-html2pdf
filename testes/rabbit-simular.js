#!/usr/bin/env node

import cliente from './rabbit-cliente'

const idReq = Math.floor(Math.random() * 10000) + 1

cliente.enviarMensagem(idReq, function () {
  console.log('Prova enviada.')
})