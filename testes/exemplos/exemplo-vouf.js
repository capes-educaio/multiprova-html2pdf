import gerarExemplo from './gerador-exemplo'
import questaovouf from '../data/exemplo-questao-vouf.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: questaovouf,
  template: enumTipoTemplate.voufGeral,
  nomeArquivo: 'questao-vouf',
})
