import gerarExemplo from './gerador-exemplo'
import caderno from '../data/exemplo-caderno.json'
import { enumTipoTemplate } from 'utils/enumTipoTemplate'

gerarExemplo({
  dados: caderno,
  template: enumTipoTemplate.cadernoComperve,
  nomeArquivo: 'caderno',
})
