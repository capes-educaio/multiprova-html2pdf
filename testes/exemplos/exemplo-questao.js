import gerarExemplo from './gerador-exemplo'
import questao from '../data/exemplo-questao.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: questao,
  template: enumTipoTemplate.multiplaEscolhaComperve,
  nomeArquivo: 'questao',
})
