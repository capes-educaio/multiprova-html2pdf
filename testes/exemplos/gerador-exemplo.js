import { writeFileSync } from 'fs'
import path from 'path'

import GeradorPdfSigleton from 'gerador-pdf'
import { enumTipoTemplate } from 'utils/enumTipoTemplate'
import repositorioSingleton from 'repositorio'
import { compilarHtml } from 'gerador-html'
import config from 'config'

const REPOSITORIO = repositorioSingleton.getInstance()

const criarArquivoHtml = (html, nomeArquivo) => {
  writeFileSync(`saida/${nomeArquivo}.html`, html, err => {
    if (err) throw err
  })
}

const geradorExemplo = async ({ dados, template, nomeArquivo }) => {
  const instancia = await GeradorPdfSigleton.getInstance()
  try {
    const html = compilarHtml({
      dados,
      template,
      operacao: { tipo: 'oficial' },
    })
    REPOSITORIO.inicializarDiretorioSaida()
    if (template === enumTipoTemplate.cadernoComperve) {
      criarArquivoHtml(html[0].htmlCorpo, 'capa-caderno')
      criarArquivoHtml(html[1].htmlCorpo, 'provas')
    } else {
      criarArquivoHtml(html.htmlCorpo, nomeArquivo)
    }
    const localPath = path.join(__dirname, `../../${config.dir_saida}/${nomeArquivo}.pdf`)
    await instancia.gerarPdf({ html, localPath, template, dados })
    console.log(`Arquivo ${nomeArquivo}.pdf gerado`)
  } catch (err) {
    throw err
  } finally {
    instancia.close()
    REPOSITORIO.pararMonitoramento()
  }
}

export default geradorExemplo
