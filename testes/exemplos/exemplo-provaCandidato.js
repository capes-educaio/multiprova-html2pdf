import gerarExemplo from './gerador-exemplo'
import data from '../data/exemplo-prova-candidato.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: data,
  template: enumTipoTemplate.provaGeralCandidato,
  nomeArquivo: 'prova-candidato',
})
