import gerarExemplo from './gerador-exemplo'
import prova from '../data/exemplo-prova.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: prova,
  template: enumTipoTemplate.provaGeral,
  nomeArquivo: 'prova-geral',
})
