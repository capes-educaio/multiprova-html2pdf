import gerarExemplo from './gerador-exemplo'
import questaoAssociacaoDeColunas from '../data/exemplo-questao-associacao-colunas.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: questaoAssociacaoDeColunas,
  template: enumTipoTemplate.associacaoDeColunasGeral,
  nomeArquivo: 'questao-associacao',
})
