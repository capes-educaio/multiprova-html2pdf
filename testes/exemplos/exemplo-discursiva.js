import gerarExemplo from './gerador-exemplo'
import questaodiscursiva from '../data/exemplo-questao-discursiva.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: questaodiscursiva,
  template: enumTipoTemplate.discursivaGeral,
  nomeArquivo: 'questao-discursiva',
})
