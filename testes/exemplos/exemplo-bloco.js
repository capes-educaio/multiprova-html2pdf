import gerarExemplo from './gerador-exemplo'
import bloco from '../data/exemplo-bloco.json'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

gerarExemplo({
  dados: bloco,
  template: enumTipoTemplate.blocoComperve,
  nomeArquivo: 'bloco',
})
