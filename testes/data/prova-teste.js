export default `[{
  "id": "118e1099-875c-4121-a1ce-7c28d3b19e2d",
  "dataCadastro": "2018-08-23T02:49:57.555Z",
  "dataUltimaAlteracao": "2018-08-23T02:49:57.555Z",
  "descricao": "Prova para gerar pdf",
  "titulo": "Concurso Público - Edital n° 01/2018 - SEARH - SESAP",
  "dataAplicacao": "2018-08-23T00:00:00.000Z",
  "instituicao": "Secretaria de Estado da Saúde Pública",
  "grupos": [
    {
      "questoes": [
        {
          "id": "17de8dbc-9cd4-4e5a-9c4e-ac5df9941795",
          "dataCadastro": "2018-08-18T22:06:44.774Z",
          "dataUltimaAlteracao": "2018-08-18T22:07:47.153Z",
          "enunciado": "<p>No sistema operacional MS Windows, o espaço utilizado para armazenar informações recortadas ou copiadas é chamado de:</p>",
          "dificuldade": 2,
          "tipo": "multiplaEscolha",
          "numeroNaInstancia": 1,
          "multiplaEscolha": {
            "respostaCerta": "a",
            "alternativasPorLinha": 1,
            "alternativas": [
              {
                "letraNaInstancia": "a",
                "texto": "<p> O Sistema Operacional gerencia os recursos computacionais </p>"
              },
              { "letraNaInstancia": "b", "texto": "<p> Área de armazenamento </p>" },
              { "letraNaInstancia": "c", "texto": "<p> Área de transferência </p>" },
              { "letraNaInstancia": "d", "texto": "<p>Área de cópia </p>" }
            ]
          },
          "usuarioId": "9816a87f-1a1d-4396-b70a-8122d63a3fde",
          "criadoPor": "sistema",
          "open": false
        }
      ]
    }
  ],
  "professor": "Fulano de Tals",
  "usuarioId": "9816a87f-1a1d-4396-b70a-8122d63a3fde",
  "criadoPor": "9816a87f-1a1d-4396-b70a-8122d63a3fde",
  "tema": "Assistente Técnico em Saúde",
  "tagsVirtuais": []
}]`
