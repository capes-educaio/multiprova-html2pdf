import exemploProva from './exemplo-prova'
import { enumTipoTemplate } from '../../src/utils/enumTipoTemplate'

export const getInstaciaFilaProva = () => {
  const arrayProva = []
  arrayProva.push(exemploProva)
  return {
    id: 56789,
    template: enumTipoTemplate.provaComperve,
    dados: arrayProva,
  }
}
