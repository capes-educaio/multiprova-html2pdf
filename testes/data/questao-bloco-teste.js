export default `{
  "id": "118e1099-875c-4121-a1ce-7c28d3b19e2d",
  "dataCadastro": "2018-08-23T02:49:57.555Z",
  "dataUltimaAlteracao": "2018-08-23T02:49:57.555Z",
  "descricao": "Prova para gerar pdf",
  "titulo": "Teste de bloco de questões",
  "dataAplicacao": "",
  "instituicao": "",
  "grupos": [
    {
      "questoes": [
        {
          "enunciado": "<p>&nbsp;&nbsp;&nbsp;&nbsp;Podemos afirmar que uma borboleta, um cajueiro, um cogumelo e um humano são seres vivos, enquanto uma rocha, o vento e a água não são. Fazemos isto porque os seres vivos compartilham características que os distinguem de seres não vivos. Estas características incluem determinados tipos de organização e a presença de uma variedade de reações químicas que os capacitam a manter o ambiente interno estável, mesmo quando o ambiente externo varia, permitindo-lhes obter energia, deslocar-se no ambiente, responder a estímulos provindos dele e perpetuar a vida. Para realizar tais funções, os seres vivos são compostos por unidades básicas que constituem a totalidade do seu corpo, ou estas unidades estão agregadas, formando estruturas complexas que realizam determinadas funções, como impulsionar o sangue. Estas formas vivas podem produzir outras idênticas ou muito similares a si próprias, um processo realizado por uma série de estruturas que agem em conjunto. No início de suas vidas, essas formas vivas podem ser idênticas aos organismos que as formaram ou sofrerem mudanças que as tornam similares a esses organismos num estágio posterior, além de aumentarem o tamanho dos seus corpos durante este processo.</p>",
          "tipo": "bloco",
          "bloco": {
            "fraseInicial": "Frase inicial",
            "questoes": [
              {
                "enunciado": "<p>No texto, estão citadas as conceituações das seguintes características dos seres vivos:</p>",
                "tipo": "multiplaEscolha",
                "numeroNaInstancia": 1,
                "multiplaEscolha": {
                  "alternativas": [
                    {
                      "letraNaInstancia": "a",
                      "texto": "<p>metabolismo, movimento, reatividade, crescimento, reprodução.</p>"
                    },
                    {
                      "letraNaInstancia": "b",
                      "texto": "<p>evolução, reatividade, ambiente, reprodução, crescimento.&nbsp;</p>"
                    },
                    {
                      "letraNaInstancia": "c",
                      "texto": "<p>evolução, composição química, movimento, reprodução, crescimento.&nbsp;</p>"
                    },
                    {
                      "letraNaInstancia": "d",
                      "texto": "<p>respiração, reprodução, composição química, movimento, crescimento.</p>"
                    },
                    {
                      "letraNaInstancia": "e",
                      "texto": "<p>&nbsp;metabolismo, ambiente, movimento, reatividade, crescimento.</p>"
                    }
                  ]
                }
              },
              {
                "enunciado": "<p>Os níveis de organização da vida que se podem depreender do texto são:</p>",
                "tipo": "multiplaEscolha",
                "numeroNaInstancia": 2,
                "multiplaEscolha": {
                  "alternativas": [
                    {
                      "letraNaInstancia": "a",
                      "texto": "<p>célula, órgão, população, ecossistema.</p>"
                    },
                    {
                      "letraNaInstancia": "b",
                      "texto": "<p>&nbsp;célula, órgão, sistema, organismo.&nbsp;</p>"
                    },
                    {
                      "letraNaInstancia": "c",
                      "texto": "<p>tecido, sistema, organismo, biosfera.</p>"
                    },
                    {
                      "letraNaInstancia": "d",
                      "texto": "<p>tecido, órgão, sistema, comunidade.</p>"
                    },
                    {
                      "letraNaInstancia": "e",
                      "texto": "<p>órgão, sistema, organismo, população.</p>"
                    }
                  ]
                }
              }
            ]
          }
        }
      ]
    }
  ]
}`
