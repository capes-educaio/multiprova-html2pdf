# multiprova-html2pdf

[![pipeline status](https://gitlab.com/capes-educaio/multiprova-html2pdf/badges/develop/pipeline.svg)](https://gitlab.com/tapioca/multiprova/multiprova-html2pdf/commits/develop)
[![quality gate](http://177.20.148.52/sonar/api/badges/gate?key=multiprova-html2pdf)](http://177.20.148.52/sonar/dashboard?id=multiprova-html2pdf)
[![cobertura de testes](http://177.20.148.52/sonar/api/badges/measure?key=multiprova-html2pdf&metric=coverage&blinking=true)](http://177.20.148.52/sonar/component_measures?id=multiprova-html2pdf&metric=coverage)
[![Bugs](http://177.20.148.52/sonar/api/badges/measure?key=multiprova-html2pdf&metric=bugs&blinking=true)](http://177.20.148.52/sonar/project/issues?id=multiprova-html2pdf&resolved=false&types=BUG)
[![Code Smell](http://177.20.148.52/sonar/api/badges/measure?key=multiprova-html2pdf&metric=code_smells&blinking=true)](http://177.20.148.52/sonar/project/issues?id=multiprova-html2pdf&resolved=false&types=CODE_SMELL)
[![Vunerabilidade](http://177.20.148.52/sonar/api/badges/measure?key=multiprova-html2pdf&metric=vulnerabilities&blinking=true)](http://177.20.148.52/sonar/project/issues?id=multiprova-html2pdf&resolved=false&types=VULNERABILITY)

Serviço de renderização de prova em PDF a partir de HTML utilizando o Google Chrome no modo --headless, com o auxílio da biblioteca [GoogleChrome/puppeteer](https://github.com/GoogleChrome/puppeteer).

As solicitações e resposta para geração dos PDFs são realizadas assincronamente através do broker [RabbitMQ](https://www.rabbitmq.com/).

## Instruções

1.  Clone o repositório
2.  Entre no diretório: `$ cd multiprova-html2pdf`
3.  Instale as dependências: `$ npm install`
4.  Inicie o broker localmente através do seguinte comando Docker (Linux ou MacOs): `$ rmq_container_id=$(docker run -d -p 5672:5672 -p 8080:15672 --restart always rabbitmq) && sleep 10 && docker exec $rmq_container_id rabbitmq-plugins enable rabbitmq_management`
5.  No windows utilize o seguinte comando: `$ docker run -d -p 5672:5672 -p 8080:15672 --restart always rabbitmq`
6.  Inicie o serviço através do comando `$ npm start`

## Executar com Docker

Além da execução local, é possíivel também executar via Docker. Para isso, existem duas diferentes possibilidades:

1. Criar a imagem localmente e executá-la
2. Executar imagem registrada no registro do Gitlab

### Criar imagem localmente

Esta opção é útil quando se está desenvolvendo o serviço e deseja testar no mesmo ambiente que será posto em produção.
Além disso, como este projeto utiliza o Chrome headless, alguns sistemas operacionais não são plenamentes compatíveis. Nestes casos, o Docker se torna uma excelente alternativa.

Dito isso, para criar e executar a imagem localmente, siga os seguintes passos:

1. Crie a imagem com o comando: `$ docker build -t multiprova-html2pdf .`
2. Execute a imagem: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3000:3000 multiprova-html2pdf`

Importante: Lembre-se de trocar **HOST_BROKER** pelo hostname do broker. Não utilize localhost, pois, neste caso, o serviço não será capaz de localizar o broker. Em vez disso, utilize o nome da máquina ou IP local do servidor do broker.

### Executar imagem do Gitlab

Caso precise apenas executar o serviço, esta se torna uma excelente alternativa. Neste caso, não é necessário realizar a clonagem do projeto, pois serão utilizadas as imagens registradas pelo pipeline do projeto.

Para isso, siga o seguinte passo a passo:

1. Faça login no Docker do Gitlab: `docker login registry.gitlab.com/tapioca/multiprova/multiprova-html2pdf`
2. Para executar a imagem do último pipeline de desenvolvimento: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3000:3000 registry.gitlab.com/tapioca/multiprova/multiprova-html2pdf:dev`
3. Para executar a imagem da última versão estável: `docker run -i -d --cap-add=SYS_ADMIN -e RABBIT_SERVER_URL=amqp:HOST_BROKER -p 3000:3000 registry.gitlab.com/tapioca/multiprova/multiprova-html2pdf:latest`

Importante: Lembre-se de trocar **HOST_BROKER** pelo hostname do broker. Não utilize localhost, pois, neste caso, o serviço não será capaz de localizar o broker. Em vez disso, utilize o nome da máquina ou IP local do servidor do broker.

### Configurações disponíveis

É possível alterar uma série de parâmetros do sistema atravéz da definição de variáveis de ambiente. A tabela abaixo lista as variáveis disponíveis:

| **Variável**                         | **Descrição**                                                                                                         |
| ------------------------------------ | --------------------------------------------------------------------------------------------------------------------- |
| RABBIT_SERVER_URL                    | Url do servidor do broker RabbitMQ. Valor padrão: amqp://localhost                                                    |
| RABBIT_SERVER_TESTE_URL              | Url do servidor do broker RabbitMQ a ser utilizado no teste de integração. Valor padrão: amqp://localhost             |
| WEB_PROTOCOLO                        | Protocolo (http ou https) a ser utilizado para o download dos arquivos. Valor padrão: http                            |
| WEB_PORT                             | Porta a ser utilizada para servir os arquivos renderizados. Valor padrão: 3000                                        |
| API_HOSTNAME                         | Hostname do host a ser utilizado para baixar os arquivos. Valor padrão: localhost                                     |
| TEMPO_EXPIRACAO_POS_DOWNLOAD_MINUTOS | Tempo em minutos a ser aguardado após o download do arquivo para o mesmo ser removido em definitivo. Valor padrão: 10 |
| TEMPO_EXPIRACAO_MINUTOS              | Tempo em minutos a ser aguardado, independente de ocorrer o download, para a remoção do arquivo. Valor padrão: 20     |
| MONITORAMENTO_EXPIRACAO_MINUTOS      | Período de tempo (em minutos) para checagem se há arquivos a serem removidos. Valor padrão: 2                         |

## Testes

Caso queira simular o funcionamento do serviço, execute o `$ npm run simular-envio`. Este script simula o envio uma prova ao serviço.

Após o arquivo PDF ter sido gerado, o download pode ser feito pela seguinte rota: http://localhost:3000/download/:id

O :id desverá ser substituido pelo id da requisição gerada ao serviço.

Caso queira chamar um teste parar geração de pdfs independentes de toda comunicação, é possível utilizar os seguintes comandos:

- Prova comum: `$ npm run exemplo-prova`
- Preview de questão: `$ npm run exemplo-questao`
- Preview de bloco: `$ npm run exemplo-bloco`
- Preview de questão de associação de colunas: `$ npm run exemplo-associacaoDeColunas`
- Preview de questão discursiva: `$ npm run exemplo-discursiva`
- Preview de questão vouf: `$ npm run vouf`

## Administração do Broker

Caso tenha executado o broker localmente e queira monitorá-lo, acesse o painel de administração através do seguinte endereço: http://localhost:8080/

Utilize as seguintes credenciais:

- Usuário: guest
- Senha: guest
