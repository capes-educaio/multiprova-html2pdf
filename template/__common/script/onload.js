document.addEventListener('DOMContentLoaded', function() {
  renderMath()
  handleImageAtTable()
})

var renderMath = function() {
  renderMathInElement(document.body, {
    delimiters: [
      { left: '/block_tex/', right: '/block_tex/', display: true },
      { left: '/inline_tex/', right: '/inline_tex/', display: false },
    ],
  })
}

var handleImageAtTable = function() {
  var images = document.querySelectorAll('.table .image')
  images.forEach(image => {
    var node = document.createElement('DIV')
    node.innerHTML = '.'
    node.style.width = '0px'
    image.prepend(node)
  })
}
