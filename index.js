import { Consumidor } from 'consumidor'
import config from 'config'
import app from 'app'

const server = app.listen(config.web.port, function() {
  console.log(
    'API do serviço de renderização de provas iniciada na porta %d.',
    config.web.port
  )

  console.log('Controle de atualização')

  const consumidor = new Consumidor()

  consumidor.iniciarConsumidor(
    function() {
      console.log('Sucesso ao iniciar o consumidor do broker.')
    },
    function(err) {
      console.error(
        `Falha durante a inicialização do consumidor. O serviço será encerrado. ${err}`
      )
      process.exit(1)
    }
  )
})

export default server
