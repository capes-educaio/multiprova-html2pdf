export const gerarPreviewImagem = async ({ localPath, template }) => {
  console.log(`Gerando preview de ${template}.`)
  const util = require('util')
  const execFile = util.promisify(require('child_process').execFile)

  const localPathPng = localPath.replace('.pdf', '.png')
  var comando = 'convert'
  var args = [
    '-density',
    '100',
    '-crop',
    '826x1100+0+0',
    '+repage',
    '-trim',
    '+repage',
    '-sharpen',
    '0x0.1',
    localPath,
    '-append',
    '-bordercolor',
    'white',
    '-border',
    '20',
    localPathPng,
  ]
  await execFile(comando, args)
  return { localPath: localPathPng }
}
