import { unlinkSync, existsSync, mkdirSync } from 'fs'
import { join } from 'path'
import findRemoveSync from 'find-remove'
import config from './config'

class Repositorio {
  constructor() {
    this.localPath = join(__dirname, '../saida/')
    this.repositorio = new Map()
    this.status = {
      NOVO: 'NOVO',
      DOWNLOADED: 'DOWNLOADED',
    }

    this.inicializarDiretorioSaida()

    console.log(
      `Iniciando monitoramento de arquivos expirados a cada ${config.monitoramento_expiracao_minutos} minuto(s)`
    )
    this.intervalId = setInterval(() => this.removerArquivosExpirados(), config.monitoramento_expiracao_minutos * 60)
  }

  adicionarArquivo(id, localPath, url) {
    var dado = {
      id: id,
      localPath: localPath,
      url: url,
      status: this.status.NOVO,
      criacao: Date.now(),
      ultimaAtualizacao: Date.now(),
    }
    this.repositorio.set(id.toString(), dado)
  }

  getArquivoById(id) {
    return this.repositorio.get(id.toString())
  }

  atualizarStatus(id, status) {
    var idInt = id.toString()
    const arquivo = this.repositorio.get(idInt)
    if (arquivo) {
      arquivo.status = status
      arquivo.ultimaAtualizacao = Date.now()
      this.repositorio.set(idInt, arquivo)
    }
  }

  removerArquivosExpirados() {
    for (var [key, value] of this.repositorio) {
      if (this.estaExpirado(value)) {
        console.log(`Arquivo ${value.localPath} expirado! Será removido.`)
        try {
          unlinkSync(value.localPath)
          this.repositorio.delete(key)
          console.log(`Sucesso ao deletar o arquivo ${value.localPath}`)
        } catch (err) {
          console.error(`Falha ao tentar deletar o arquivo ${value.localPath}. Detalhes a seguir`)
          console.error(err)
        }
      }
    }
  }

  estaExpirado(arquivo) {
    const janelaPosDownload = config.tempo_expiracao_pos_download_minutos * 60 * 1000
    const janelaExpiracao = config.tempo_expiracao_minutos * 60 * 1000
    const timeAtual = Date.now()

    const condicao1 =
      arquivo.ultimaAtualizacao < timeAtual - janelaPosDownload && arquivo.status === this.status.DOWNLOADED
    const condicao2 = arquivo.criacao < timeAtual - janelaExpiracao

    return condicao1 || condicao2
  }

  inicializarDiretorioSaida() {
    if (!existsSync(this.localPath)) {
      console.log(`Direterio ${this.localPath} inexistente. Será criado.`)
      mkdirSync(this.localPath)
    }

    this.removerArquivosGerados()
  }

  removerArquivosGerados() {
    console.log(`Limpando diretório de saída caso haja arquivos remanescentes. Diretório de saída: ${this.localPath}`)
    findRemoveSync(this.localPath, {
      extensions: ['.pdf', '.PDF', '.png', '.PNG'],
    })
  }

  pararMonitoramento() {
    console.log(`Parando monitoramento do serviço de remoção de ID ${this.intervalId}.`)
    clearInterval(this.intervalId)
  }
}

let instance

class RepositorioSigleton {
  static getInstance() {
    if (!instance) {
      instance = new Repositorio()
    }

    return instance
  }
}

export default RepositorioSigleton
