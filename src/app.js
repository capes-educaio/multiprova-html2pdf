import express from 'express'
import { existsSync } from 'fs'
import RepositorioSingleton from 'repositorio'

const app = express()
const repositorio = RepositorioSingleton.getInstance()

app.get('/', function(req, res) {
  res.send('Bem vindo a API para geração de PDFs =)')
})

app.get('/download/:id', function(req, res) {
  const arquivo = repositorio.getArquivoById(req.params.id)

  if (arquivo && existsSync(arquivo.localPath)) {
    res.download(arquivo.localPath)
    repositorio.atualizarStatus(req.params.id, repositorio.status.DOWNLOADED)
  } else {
    const resposta = { error: 'Arquivo não encontrado', cod: 404 }
    res.setHeader('Content-Type', 'application/json')
    res.status(404).send(JSON.stringify(resposta))
  }
})

export default app
