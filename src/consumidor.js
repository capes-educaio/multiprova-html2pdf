import amqp from 'amqplib/callback_api'

import config from 'config'
import RepositorioSingleton from 'repositorio'

import { processarMensagem } from './processarMensagem'

const QUEUE_REQUEST = 'request-render-pdf'
const URL_RABBIT = config.rabbit.server_url

const REPOSITORIO = RepositorioSingleton.getInstance()

export class Consumidor {
  conexao = null
  gerador = null
  canal = null
  filaMensagens = []

  acionarFilaDeMensagens() {
    if (this.filaMensagens.length > 0 && !this.mensagemEmProcessamento) {
      const mensagem = this.filaMensagens.shift()
      this.processarMensagem(mensagem)
    }
  }

  async processarMensagem(mensagem) {
    this.mensagemEmProcessamento = true
    try {
      console.log(`Iniciando processamento da mensagem (${mensagem.id}).`)
      console.log(`Operação da mensagem: ${JSON.stringify(mensagem.operacao)}`)
      await processarMensagem({ mensagem, canal: this.canal })
      console.log(`Mensagem (${mensagem.id}) processada com sucesso.`)
      console.log(`Operação da mensagem: ${JSON.stringify(mensagem.operacao)}`)
    } catch (error) {
      console.error(`Erro no processamento da mensagem (${mensagem.id}) .`)
      console.error(`Operação da mensagem: ${JSON.stringify(mensagem.operacao)}`)
      console.error(error)
    }
    this.mensagemEmProcessamento = false
    this.acionarFilaDeMensagens()
  }

  iniciarConsumidor(sucess, error) {
    console.log('Conectando ao servidor do RabbitMQ no seguinte endereço: %s', URL_RABBIT)
    console.log('Inicilizando o um consumidor para a fila %s.', QUEUE_REQUEST)
    amqp.connect(URL_RABBIT, (err, conexao) => {
      if (err) {
        console.error(
          'Falha ao tentar se conectar com o broker na seguinte URL ' +
            URL_RABBIT +
            'O serviço do Rabbit está rodando? Cheque o serviço se a URL e porta estão corretos.'
        )
        error(err)
      } else {
        this.conexao = conexao
        console.log('Conectado ao servidor do Rabbit para a fila %s. Irá criar o canal.', QUEUE_REQUEST)
        this.criarCanal(conexao, sucess, error)
      }
    })
  }

  shutdownConsumidor() {
    console.log(`Desligando o consumidor.`)
    if (this.conexao) this.conexao.close(this.fecharConexao)
    else console.error('Tentando desligar conexao não existente')
  }

  fecharConexao = () => {
    REPOSITORIO.pararMonitoramento()
    REPOSITORIO.removerArquivosGerados()
    if (this.gerador) this.gerador.close()
    else console.error('Tentando desligar gerador não existente')
  }

  criarCanal(conexao, sucess, error) {
    conexao.createChannel((erro, canal) => {
      if (erro) {
        error(erro)
      } else {
        this.canal = canal
        console.log('Canal de comunicação criado para a fila %s.', QUEUE_REQUEST)
        canal.assertQueue(QUEUE_REQUEST, { durable: true })
        canal.prefetch(1)
        console.log('Aguardando mensagens para a fila %s.', QUEUE_REQUEST)
        this.registrarConsumidor(this.canal)
        sucess()
      }
    })
  }

  registrarConsumidor() {
    this.canal.consume(QUEUE_REQUEST, this.consumir, { noAck: false })
  }

  consumir = async instanciaFila => {
    this.canal.ack(instanciaFila)
    const mensagem = JSON.parse(instanciaFila.content)
    this.filaMensagens.push(mensagem)
    this.acionarFilaDeMensagens()
  }
}
