import puppeteer from 'puppeteer'
import merge from 'easy-pdf-merge'
import { unlinkSync } from 'fs'
import { enumTipoTemplate } from 'utils/enumTipoTemplate'
import { inserirRodapeCadernoComperve } from 'utils/inserirRodapeCadernoComperve'

var instance

class GeradorPdf {
  constructor(browser) {
    this.browser = browser
  }

  async gerarPdfIndividual(html, pathArquivo) {
    const { htmlCorpo, htmlRodape } = html
    const page = await this.browser.newPage()
    await page.setContent(htmlCorpo, { waitUntil: 'networkidle2' })
    await page.pdf({
      path: pathArquivo,
      footerTemplate: htmlRodape,
      displayHeaderFooter: !!htmlRodape,
      headerTemplate: '<span></span>',
      margin: {
        top: '2cm',
        bottom: '1.5cm',
        right: '2cm',
        left: '2cm',
      },
      printBackground: true,
      format: 'A4',
    })
    page.close()
  }

  removerPdfsMerged(pdfFiles) {
    try {
      pdfFiles.forEach(file => {
        unlinkSync(file)
      })
    } catch (error) {
      throw new Error('Erro ao remover os pdfs merged.')
    }
  }

  async mergePDF(pdfFiles, pathArquivo) {
    return new Promise((resolve, reject) => {
      merge(pdfFiles, pathArquivo, err => {
        if (err) {
          console.log('Erro no merge dos PDFs', err)
          console.log('Arquivos que houve tentativa de realizar o merge: ', pdfFiles)
          reject(err)
        }
        resolve()
      })
    })
  }

  async gerarPdfsInstancias(htmls, pathArquivo) {
    const pdfFiles = []
    let index = 0
    for (const html of htmls) {
      const pdfFileName = `${pathArquivo}${index++}.pdf`
      pdfFiles.push(pdfFileName)
      console.log(`Gerando pdf: ${pdfFileName}`)
      await this.gerarPdfIndividual(html, pdfFileName)
    }
    await this.mergePDF(pdfFiles, pathArquivo)
    this.removerPdfsMerged(pdfFiles)
  }

  async gerarPdf({ html, localPath, template, dados }) {
    if (Array.isArray(html)) {
      console.log(`Quantidade de htmls gerados a partir das instancias: ${html.length}`)
      await this.gerarPdfsInstancias(html, localPath)
    } else {
      await this.gerarPdfIndividual(html, localPath)
    }
    if (template === enumTipoTemplate.cadernoComperve) {
      await inserirRodapeCadernoComperve(localPath, dados.fraseDeRodape)
    }
  }

  async close() {
    await this.browser.close()
  }
}

class GeradorPdfSigleton {
  static async getInstance() {
    if (!instance) {
      try {
        const browser = await puppeteer.launch()
        instance = new GeradorPdf(await Promise.resolve(browser))
      } catch (e) {
        console.log('Unable to launch browser mode in sandbox mode. Lauching Chrome without sandbox.')
        const browser = await puppeteer.launch({ args: ['--no-sandbox'] })
        instance = new GeradorPdf(await Promise.resolve(browser))
      }
    }
    return instance
  }
}

export default GeradorPdfSigleton
