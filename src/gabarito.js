import { sortAlphaNumeric } from './utils/sortAlphaNumeric'

const ORDENAR_CARGOS_POR = 'titulo'
const ORDENAR_ETAPAS_POR = 'titulo'
const ORDENAR_CADERNO_POR = 'titulo'
const ORDENAR_VERSOES_DE_CADERNO_POR = 'versaoCaderno.apelido'

const getTabelaDoCaderno = ({ versoes }) => {
  const { etapa, cargo, caderno } = versoes[0]
  const versoesSorted = sortAlphaNumeric(versoes, ORDENAR_VERSOES_DE_CADERNO_POR)
  const header = []
  const linhas = []
  versoesSorted.forEach((gabarito, indexColuna) => {
    header[indexColuna] = gabarito.versaoCaderno.apelido
    gabarito.respostas.forEach((resposta, indexLinha) => {
      if (!linhas[indexLinha]) linhas[indexLinha] = []
      linhas[indexLinha][indexColuna] = resposta
    })
  })
  return { header, linhas, etapa, cargo, caderno }
}

const getTabelasDaEtapa = ({ cadernosMap }) => {
  const cadernos = []
  const tabelas = []
  for (let cadernoId in cadernosMap) cadernos.push(cadernosMap[cadernoId])
  const cadernosSorted = sortAlphaNumeric(cadernos, ORDENAR_CADERNO_POR)
  cadernosSorted.forEach(caderno => tabelas.push(getTabelaDoCaderno(caderno)))
  return tabelas
}

const getTabelasDoCargo = ({ etapasMap }) => {
  const etapas = []
  const tabelas = []
  for (let etapaId in etapasMap) etapas.push(etapasMap[etapaId])
  const etapasSorted = sortAlphaNumeric(etapas, ORDENAR_ETAPAS_POR)
  etapasSorted.forEach(etapa => tabelas.push(...getTabelasDaEtapa(etapa)))
  return tabelas
}

export const getCargosMap = gabaritos => {
  const cargosMap = {}
  gabaritos.forEach(gabarito => {
    if (!cargosMap[gabarito.cargo.id]) cargosMap[gabarito.cargo.id] = { ...gabarito.cargo, etapasMap: {} }
    const { etapasMap } = cargosMap[gabarito.cargo.id]
    if (!etapasMap[gabarito.etapa.id]) etapasMap[gabarito.etapa.id] = { ...gabarito.etapa, cadernosMap: {} }
    const { cadernosMap } = etapasMap[gabarito.etapa.id]
    if (!cadernosMap[gabarito.caderno.id]) cadernosMap[gabarito.caderno.id] = { ...gabarito.caderno, versoes: [] }
    const { versoes } = cadernosMap[gabarito.caderno.id]
    versoes.push(gabarito)
  })
  return cargosMap
}

export const getTabelas = gabaritos => {
  const cargosMap = getCargosMap(gabaritos)
  const cargos = []
  const tabelas = []
  for (let cargoId in cargosMap) cargos.push(cargosMap[cargoId])
  const cargosSorted = sortAlphaNumeric(cargos, ORDENAR_CARGOS_POR)
  cargosSorted.forEach(cargo => tabelas.push(...getTabelasDoCargo(cargo)))
  return tabelas
}
