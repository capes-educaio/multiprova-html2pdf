const getDeepValue = (content, keySplitArg) => {
  const keySplit = [...keySplitArg]
  let value = content[keySplit[0]]
  keySplit.shift()
  keySplit.forEach(key => {
    if (value[key]) value = value[key]
    else value = ''
  })
  return value
}

const compareAlphaNumeric = key => (a, b) => {
  let valueA, valueB
  if (key) {
    const keySplit = key.split('.')
    valueA = getDeepValue(a, keySplit) ? getDeepValue(a, keySplit) : ''
    valueB = getDeepValue(b, keySplit) ? getDeepValue(b, keySplit) : ''
  } else {
    valueA = a ? a : ''
    valueB = b ? b : ''
  }
  return valueA.localeCompare(valueB, 'pt-BR', { numeric: true })
}

export const sortAlphaNumeric = (arrayArg, key) => {
  const array = [...arrayArg]
  array.sort(compareAlphaNumeric(key))
  return array
}
