import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
import { promisify } from 'util'
import { readFile, writeFile } from 'fs'

const getPdfBytes = path => promisify(readFile)(path)
const substituirArquivo = (path, data) => promisify(writeFile)(path, data)
const isOdd = number => number % 2 !== 0

export const inserirRodapeCadernoComperve = async (path, frase) => {
  const existingPdfBytes = await getPdfBytes(path)
  const pdfDoc = await PDFDocument.load(existingPdfBytes)
  const font = await pdfDoc.embedFont(StandardFonts.Helvetica)
  const textSize = 7
  const xInicial = 57
  const xFinal = 538
  const fraseWidth = font.widthOfTextAtSize(frase, textSize)
  const pages = pdfDoc.getPages()
  pages.slice(2).forEach((page, index) => {
    const numeroPagina = index + 1
    const numeroWidth = font.widthOfTextAtSize(numeroPagina.toString(), textSize)
    const drawTextOptions = {
      y: 27,
      font,
      size: textSize,
      color: rgb(0, 0, 0),
    }
    page.drawText(numeroPagina.toString(), {
      x: !isOdd(numeroPagina) ? xInicial : xFinal - numeroWidth,
      ...drawTextOptions,
    })
    page.drawText(frase, {
      x: isOdd(numeroPagina) ? xInicial : xFinal - fraseWidth,
      ...drawTextOptions,
    })
  })
  const pdfBytes = await pdfDoc.save()
  await substituirArquivo(path, pdfBytes)
}
