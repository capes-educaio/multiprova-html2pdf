import { sortAlphaNumeric } from '../sortAlphaNumeric'

describe('sortAlphaNumeric', () => {
  it('ordena corretamente sem chave', () => {
    const array = ['2', '1', '3']
    const arrayOrdenado = sortAlphaNumeric(array)
    expect(arrayOrdenado).toEqual(['1', '2', '3'])
  })

  it('ordena corretamente, com chave, sem profundidade', () => {
    const array = [{ chave: '2' }, { chave: '1' }, { chave: '3' }]
    const arrayOrdenado = sortAlphaNumeric(array, 'chave')
    expect(arrayOrdenado).toEqual([{ chave: '1' }, { chave: '2' }, { chave: '3' }])
  })

  it('ordena corretamente, com chave, com profundidade', () => {
    const array = [{ chave: { profunda: '2' } }, { chave: { profunda: '1' } }, { chave: { profunda: '3' } }]
    const arrayOrdenado = sortAlphaNumeric(array, 'chave.profunda')
    expect(arrayOrdenado).toEqual([
      { chave: { profunda: '1' } },
      { chave: { profunda: '2' } },
      { chave: { profunda: '3' } },
    ])
  })
})
