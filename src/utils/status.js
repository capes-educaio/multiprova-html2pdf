export const STATUS = {
  fila: 'EM FILA',
  processandoHtml: 'PROCESSANDO HTML',
  preparandoPdf: 'PREPARANDO PDF',
  gerandoPdf: 'GERANDO PDF',
  pronto: 'PRONTO',
  error: 'ERROR',
}
