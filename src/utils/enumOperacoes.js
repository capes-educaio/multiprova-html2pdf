export const enumOperacoes = Object.freeze({
  rascunho: 'rascunho',
  oficial: 'oficial',
  cadernoDeConcurso: 'cadernoDeConcurso',
  gabaritosConcurso: 'gabaritosConcurso',
})
