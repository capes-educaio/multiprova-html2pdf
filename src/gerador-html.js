import pug from 'pug'
import { enumOperacoes } from './utils/enumOperacoes'
import { enumTipoTemplate } from 'utils/enumTipoTemplate'
import { getTabelas } from './gabarito'

const getDadosParaGerarPdf = ({ template, operacao }, comRodape = true) => instancia => {
  const compiledFunctionCorpo = pug.compileFile(`template/${template}.pug`)
  const compiledFunctionRodape = comRodape
    ? pug.compileFile(`template/${template.split('/')[0]}/include/rodape.pug`)
    : _ => ''
  instancia.temMarcaDagua = operacao.informacoes && operacao.informacoes.temMarcaDagua
  return {
    htmlCorpo: compiledFunctionCorpo(instancia),
    htmlRodape: compiledFunctionRodape(instancia),
  }
}

export const compilarHtml = ({ dados, template, id, operacao }) => {
  console.log(`Gerando o html do template ${template} com os dados da mensagem (${id}).`)
  try {
    if (template === enumTipoTemplate.cadernoComperve) {
      const dadosCapa = getDadosParaGerarPdf({ template: enumTipoTemplate.capaCadernoComperve, operacao }, false)(dados)
      const dadosProvas = getDadosParaGerarPdf({ template: 'comperve/provasCaderno', operacao })(dados)
      return [dadosCapa, dadosProvas]
    } else if (operacao.tipo === enumOperacoes.gabaritosConcurso) {
      let tabelas = getTabelas(dados)
      return getDadosParaGerarPdf({ template, operacao })({ tabelas })
    } else if (operacao.informacoes && operacao.informacoes.tipoDados === 'questao') {
      const { htmlCorpo } = getDadosParaGerarPdf({ template, operacao })(dados)
      return { htmlCorpo }
    } else if (Array.isArray(dados)) {
      return dados.map(getDadosParaGerarPdf({ template, operacao }))
    } else {
      return getDadosParaGerarPdf({ template, operacao })(dados)
    }
  } catch (err) {
    console.error(err)
    throw new Error('Erro ao compilar o html')
  }
}
