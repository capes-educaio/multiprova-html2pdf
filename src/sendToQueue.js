import uid from 'uuid/v4'
import { STATUS } from 'utils/status'

const QUEUE_RESPONSE = 'response-render-pdf'

export const sendToQueue = ({ ch, status, url, operacao, impressaoId }) => {
  ch.sendToQueue(QUEUE_RESPONSE, Buffer.from(JSON.stringify({ id: uid(), status, url, operacao, impressaoId })), {
    persistent: true,
  })
  const stringComplemento = status !== STATUS.error ? `instancia que será disponibilizada na seguinte URL: ${url}` : ''
  console.log(`Mandando para fila novo status (${status}) ${stringComplemento}`)
}
