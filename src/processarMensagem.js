import shortid from 'shortid'
import path from 'path'

import config from 'config'
import geradorPdf from 'gerador-pdf'
import RepositorioSingleton from 'repositorio'
import { compilarHtml } from 'gerador-html'
import { STATUS } from 'utils/status'

import { sendToQueue } from './sendToQueue'
import { gerarPreviewImagem } from './gerarPreviewImagem'

const QUEUE_RESPONSE = 'response-render-pdf'

const REPOSITORIO = RepositorioSingleton.getInstance()

const getDados = ({ id }) => {
  const filename = shortid.generate().concat('.pdf')
  const localPath = path.join(__dirname, `../${config.dir_saida}/`).concat(filename)
  const url = `${config.web.protocolo}://${config.web.hostname}:${config.web.port}/download/${id}`
  return { localPath, url }
}

const enviarParaFilaResposta = ({ canal, operacao, impressaoId, url }) => async ({ status }) => {
  canal.assertQueue(QUEUE_RESPONSE, { durable: true })
  sendToQueue({ ch: canal, status, url, operacao, impressaoId })
}

const gerarArquivo = async ({ html, localPath, template, operacao, dados }) => {
  const gerador = await geradorPdf.getInstance()
  await gerador.gerarPdf({ html, localPath, template, dados })
  if (operacao.informacoes && operacao.informacoes.extensao === 'png') {
    return await gerarPreviewImagem({ localPath, template })
  } else {
    return { localPath }
  }
}

export const processarMensagem = async ({ mensagem, canal }) => {
  const { dados, id, template, operacao, impressaoId } = mensagem
  let { localPath, url } = getDados({ id })
  const enviarComStatus = enviarParaFilaResposta({ canal, template, operacao, impressaoId, url })
  const html = compilarHtml({ dados, template, id, operacao })
  ;({ localPath } = await gerarArquivo({ html, localPath, template, operacao, dados }))
  REPOSITORIO.adicionarArquivo(id, localPath, url)
  enviarComStatus({ status: STATUS.pronto })
}
